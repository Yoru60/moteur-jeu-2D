from pygame.locals import *
import pygame
#C'est ici que devront se trouver toute les classes
class Exemple(pygame.sprite.Sprite):
    def __init__(self):
        self.image = pygame.image.load("./test.png").convert_alpha() #l'image
        self.rect = self.image.get_rect() #Le rect (ce rect sera automatiquemnt déplacé vers les coordonnées obtenue à partir de la map)
        self.events = ["KEYDOWN","KEYUP", "OUTSCREEN", "INSCREEN", "RECTCOLLIDE"] #Existe en event : KEYDOWN, KEYUP, COLLIDRECT,OUTSCREEN, INSCREEN
        self.window = Game.window
        #Fin des variables obligatoires
        self.sens = "centre"
        self.Inscreen = True
    def KEYDOWN(self, event): #KEYDOWN et KEYUP doivent avoir un parametre event (variable d'événement, ici une touche)
        if event.key == K_RIGHT: #Les deplacements exemples:
            self.sens = "droite"
        elif event.key == K_LEFT:
            self.sens = "gauche"
        elif event.key == K_DOWN:
            self.sens = "bas"
        elif event.key == K_UP:
            self.sens = "haut"
        elif event.key == K_q: #Replacement du personnage à l'origine par la touche "a" sur azerty ou "q" sur qwerty
            self.rect.x = 0
            self.rect.y = 0
        elif event.key == K_ESCAPE:
            Game.quit = 1
        elif event.key == K_F11:
            Game.icon = "./test.png"
    def KEYUP(self,event):
        if event.key == K_RIGHT and self.sens == "droite":
            self.sens = "centre"
        if event.key == K_LEFT and self.sens == "gauche":
            self.sens = "centre"
        if event.key == K_UP and self.sens == "haut":
            self.sens = "centre"
        if event.key == K_DOWN and self.sens == "bas":
            self.sens = "centre"
    def OUTSCREEN(self): #Indique à l'objet si il est hors de la fenetre
        if self.Inscreen == True:
            print("OUT !")
            self.Inscreen = False
    def INSCREEN(self): #Indique à l'objet si il est dans la fenetre
        if self.Inscreen == False:
            print("IN ! ")
            self.Inscreen = True
    def RECTCOLLIDE(self, CollideList, IndexList): #L'event de collision (cette fonction) ne sera pas appelé car non spécifié dans la variable events
        print("Je touche quelque chose")
    def update(self, spriteList): #Cette fonction sera appelé à chaque image du jeu (100 images par secondes en principe)
        if self.sens == "gauche":
            self.rect.move_ip(-5,0)
        if self.sens == "droite":
            self.rect.move_ip(5,0)
        if self.sens == "haut":
            self.rect.move_ip(0,-10)
        if self.sens == "bas":
            self.rect.move_ip(0,10)
        self.window.blit(self.image, self.rect) #On affiche sur la fenetre definie à la déclaration de l'objet
class Game:
    #Toutes ces variables sont obligatoires
    icon = "./test.jpg"
    modeFenetre = None #https://www.pygame.org/docs/ref/display.html#pygame.display.set_mode
    tailleFenetre = (800,800)
    FPS = 100 #Nombre d'image par seconde
    objectsHorsMap = {Exemple : 1} #La liste des objets qui ne seront pas présents sur la map {Objet : Nombre de fois}
    objectsMap = {"1":Exemple} #On définie à quoi correspond telle ou telle chose
    level = ["    1",
             "",
             "",
             ""] #La carte du niveau, chaque caractere fait tailleCase x tailleCase
    tailleCase = 60 #La taille des cases (tailleCase x tailleCase)
    quit = 0
    #Les Fonctions obligatiores :
    def debut():  #Cette fonction est appele au début du jeu
        print("Début du jeu")
        pass
    def fin(): #Cette fonction est appelee à la fin du jeu (lorsque Game.fin > 0 ou que la fenetre est fermée)
        print("Fin du jeu")
    def update(spriteList): #Cette fonction update sera appelé avant toute les autres
        pygame.draw.rect(Game.window, Color("Black"),pygame.Rect((0,0),Game.window.get_rect().size))
