from pygame.locals import *
import pygame
import objects
def creation_map(Game):
    liste = []
    collisionList = []
    for y in range(0,len(Game.level)):
        for x in range(0,len(Game.level[y])):
            objet = Game.level[y][x]
            if objet in Game.objectsMap:
                objet = Game.objectsMap[objet]()
                objet.rect.x = x*Game.tailleCase
                objet.rect.y = y*Game.tailleCase
                try:
                    if i.haut == True:
                        liste.append(0,objet)
                    else:
                        liste += [objet]
                except:
                    liste += [objet]
                if "RECTCOLLIDE" in objet.events:
                    collisionList += [liste.index(objet)]
    for i in Game.objectsHorsMap:
        objet = i()
        number = Game.objectsHorsMap[i]
        for y in range(0,number):
            liste += [objet]
            if "RECTCOLLIDE" in objet.events:
                collisionList += [liste.index(objet)]
    return liste,collisionList
#On creer la fenetre sous le nom de screen
Game = objects.Game
tailleFenetre = Game.tailleFenetre
try:
    Icone = Game.icon
except:
    Icone = None
try:
    modeFenetre = Game.modeFenetre
except:
    modeFenetre = None
if modeFenetre!= None:
    screen = pygame.display.set_mode(Game.tailleFenetre, Game.modeFenetre)
else:
    screen = pygame.display.set_mode(Game.tailleFenetre)
Game.window = screen
loop = 1
Game.debut()
HorsMap = Game.objectsHorsMap
surMap = Game.objectsMap
Level = Game.level
Game.liste, Game.collisionList = creation_map(Game)
try:
    pygame.display.set_icon(pygame.image.load(Game.icon).convert_alpha())
except:
    pass
clock = pygame.time.Clock()
while loop:
    clock.tick(Game.FPS)
    for event in pygame.event.get():
        if event.type == QUIT:
            Game.quit = 1
        if event.type == KEYDOWN:
            for i in Game.liste:
                if "KEYDOWN" in i.events:
                    i.KEYDOWN(event)
        if event.type == KEYUP:
            for i in Game.liste:
                if "KEYUP" in i.events:
                    i.KEYUP(event)
    for i in Game.collisionList:
        tempRect = Game.liste[i]
        del(Game.liste[i])
        rectlist = []
        for a in Game.liste:
            rectlist += [a.rect]
        Game.liste.insert(i, tempRect)
        collisions = Game.liste[i].rect.collidelistall(rectlist)
        if collisions != []:
            Game.liste[i].RECTCOLLIDE(Game.liste,collisions)
    Game.update(Game.liste)
    for i in Game.liste:
        if "OUTSCREEN" in i.events and ((i.rect.y < 0 or i.rect.y > Game.tailleFenetre[1]) or (i.rect.x < 0 or i.rect.x > Game.tailleFenetre[0])):
            i.OUTSCREEN()
        elif "INSCREEN" in i.events:
            i.INSCREEN()
        i.update(spriteList = Game.liste)
    if Game.tailleFenetre != tailleFenetre or Game.modeFenetre != modeFenetre:
        tailleFenetre = Game.tailleFenetre
        modeFenetre = Game.modeFenetre
        screen = pygame.display.set_mode(Game.tailleFenetre, Game.modeFenetre)
    try:
        if Game.icon != Icone:
            Icone = Game.icon
            pygame.display.set_icon(pygame.image.load(Game.icon).convert_alpha())
    except:
        pass
    if surMap != Game.objectsMap or HorsMap != Game.objectsHorsMap or Level != Game.level:
        liste, collisionList = creation_map(Game)
    if Game.quit:
        Game.fin()
        loop = False
    pygame.display.update()
